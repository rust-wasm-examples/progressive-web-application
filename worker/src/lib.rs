mod utils;

use wasm_bindgen::JsCast;
use wasm_bindgen_futures::JsFuture;
// use rand::{rngs::OsRng, Rng};
use js_sys::Array;
use wasm_bindgen::prelude::*;
// use wasm_bindgen::JsCast;
// use web_sys::{
//    CanvasRenderingContext2d, Document, HtmlCanvasElement, HtmlElement, HtmlImageElement,
//    HtmlInputElement, MouseEvent, Touch, TouchEvent, Window,
// };
use web_sys::{Cache, ExtendableEvent, ServiceWorkerGlobalScope};

// use std::cell::RefCell;
// use std::rc::Rc;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

thread_local! {
    static GLOBAL: ServiceWorkerGlobalScope = js_sys::global().dyn_into::<ServiceWorkerGlobalScope>().expect("should have a window in this context");
//    static NAVIGATOR: Navigator = WINDOW.with(|w|w.navigator());
//    static SERVICE_WORKER_CONTAINER: ServiceWorkerContainer = NAVIGATOR.with(|n|n.service_worker());
//    static SERVICE_WORKER_REGISTARTION: RefCell<Option<ServiceWorkerRegistration>> = Default::default();
//    static DOCUMENT: Document = WINDOW.with(|w|w.document().expect("window should have a document"));
//    static BODY: HtmlElement = DOCUMENT.with(|d|d.body().expect("document should have a body"));
}

// Called by our JS entry point to run the example
#[wasm_bindgen(start)]
pub fn run() -> Result<(), JsValue> {
    utils::set_panic_hook();
    Ok(())
}

#[wasm_bindgen]
pub fn self_oninstall(event: ExtendableEvent) {
    let promise = wasm_bindgen_futures::future_to_promise(async {
        let promise = GLOBAL.with(|g| g.caches().unwrap().open("offline"));

        let cache = Cache::from(JsFuture::from(promise).await?);

        JsFuture::from(cache.add_all_with_str_sequence(&Array::of3(
            &JsValue::from("./index.html"),
            &JsValue::from("./index.js"),
            &JsValue::from("./index_bg.wasm"),
        )))
        .await?;

        JsFuture::from(GLOBAL.with(|g| g.skip_waiting().unwrap())).await?;

        Ok(JsValue::UNDEFINED)
    });

    event.wait_until(&promise).unwrap();
}

#[wasm_bindgen]
pub fn self_onactivate(_event: ExtendableEvent) {}
#[wasm_bindgen]
pub fn self_onfetch(_event: ExtendableEvent) {}
#[wasm_bindgen]
pub fn self_onmessage(_event: ExtendableEvent) {}
#[wasm_bindgen]
pub fn self_onpush(_event: ExtendableEvent) {}
#[wasm_bindgen]
pub fn self_onpushsubscriptionchange() {}
