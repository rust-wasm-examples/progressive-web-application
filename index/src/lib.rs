mod utils;

use wasm_bindgen::JsCast;
use wasm_bindgen_futures::JsFuture;
// use rand::{rngs::OsRng, Rng};
use wasm_bindgen::prelude::*;
// use wasm_bindgen::JsCast;
// use web_sys::{
//    CanvasRenderingContext2d, Document, HtmlCanvasElement, HtmlElement, HtmlImageElement,
//    HtmlInputElement, MouseEvent, Touch, TouchEvent, Window,
// };
use web_sys::{Navigator, ServiceWorkerContainer, ServiceWorkerRegistration, Window};

use std::cell::RefCell;
// use std::rc::Rc;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

thread_local! {
    static WINDOW: Window = web_sys::window().expect("should have a window in this context");
    static NAVIGATOR: Navigator = WINDOW.with(|w|w.navigator());
    static SERVICE_WORKER_CONTAINER: ServiceWorkerContainer = NAVIGATOR.with(|n|n.service_worker());
    static SERVICE_WORKER_REGISTARTION: RefCell<Option<ServiceWorkerRegistration>> = Default::default();
//    static DOCUMENT: Document = WINDOW.with(|w|w.document().expect("window should have a document"));
//    static BODY: HtmlElement = DOCUMENT.with(|d|d.body().expect("document should have a body"));
}

// Called by our JS entry point to run the example
#[wasm_bindgen(start)]
pub fn run() -> Result<(), JsValue> {
    utils::set_panic_hook();
    WINDOW.with(|w| {
        let closure = Closure::once_into_js(|| {
            wasm_bindgen_futures::spawn_local(async move {
                use web_sys::console::{log_1, log_2};

                let promise = SERVICE_WORKER_CONTAINER.with(|swc| swc.register("sw.js"));

                match JsFuture::from(promise).await {
                    Ok(r) => {
                        let worker = ServiceWorkerRegistration::from(r);
                        log_1(&JsValue::from_str(
                            &[
                                "ServiceWorker registration successful with scope: ",
                                &worker.scope(),
                            ]
                            .concat(),
                        ));
                        SERVICE_WORKER_REGISTARTION.with(|sw| sw.replace(Some(worker)));
                    }
                    Err(e) => {
                        log_2(
                            &JsValue::from_str("ServiceWorker registration failed: "),
                            &e,
                        );
                    }
                }
            });
        });

        w.set_onload(Some(closure.unchecked_ref()));
    });

    Ok(())
}
